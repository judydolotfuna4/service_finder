<?php include('server1.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration Form</title>
    <link rel="stylesheet" type="text/css" href="dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="asset/fontawesome/css/all.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500;600;700;800&display=swap" rel="stylesheet">   
<style>
   body {
    font-family: 'Kanit', sans-serif;
  }
  p {
    color: 	#505050;
  }
  .card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-clip: border-box;
    border-radius: .25rem;
		padding-bottom: 30px;
		box-shadow:
    0 2.8px 2.2px rgba(0, 0, 0, 0.034),
    0 6.7px 5.3px rgba(0, 0, 0, 0.048),
    0 12.5px 10px rgba(0, 0, 0, 0.06),
    0 22.3px 17.9px rgba(0, 0, 0, 0.072),
    0 41.8px 33.4px rgba(0, 0, 0, 0.086),
    0 100px 80px rgba(0, 0, 0, 0.12);
    margin-bottom: 40px;
  }
  .white-box {
    margin-bottom: 50px;
  }
   .img-placeholder {
  width: 55%;
  color: white;
  background: black;
  opacity: .7;
  height:150px;
  border-radius: 50%;
  z-index: 2;
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  display: none;
  
}
.img-placeholder h4 {
  margin-top: 40%;
  color: white;
	font-size: small;
}
.img-div:hover .img-placeholder {
  display: block;
  cursor: pointer;
}

.signup-register {
	margin-top: 50px;
}
.signup-register h2 {
	color: #2d3436;
	justify-content: center;
	font-weight: 400;
}
.card-header label {
	color: #2d3436;
	font-weight: 600;
}
.card-header span {
	color: #2d3436;
	font-weight: 600;
}
p {
  color: #2d3436;
}
select#list {
    width: 175px;
    height: 35px;
  }
  .select-brgy {
    margin-top: 10px;
  }
  .btn-register {
    background: #78e08f;
    color: #000;
    border: 1px solid rgba(116, 156, 143);
    font-size: 20px;
    padding: 2px 25px;
  }
</style>
</head>
<body class="body bg-dark">
<!-- Registration card -->
<div class="login-box signup-register">
  <section class="content">
    <div class="container">
      <!-- card -->
      <div class="card white-box">
        <div class="X" >
          <li class="nav" style="float: right; padding-right:10px"><a href="index.php"><span class="fa fa-times" style="color: black;"></span></a></li>
        </div>
        <h2><center>Employer Sign Up Form</center></h2>
        <form class="form" action="" method="post" class="form-horizontal">
          <div style="color:red;">
            <p><?php echo $errorMsg; ?></p>
          </div>
          <div class="col-md-12">
          <div class="row">
            <!-- Profile Picture -->
              <div class="col-md-3">
                <div class="form-group text-center">
                  <span class="img-div">
                  <div class="text-center img-placeholder"  onClick="triggerClick()">
                    <h4>Upload image</h4>
                  </div>
                  <img src="asset/img/Picture1.png" onClick="triggerClick()" id="profileDisplay" style="width: 150px; height: 150px" class="img rounded-circle" alt="profile"></span>
                  <input type="file" name="profilepic" onChange="displayImage(this)" id="profileImage" class="form-control" style="display: none;" required="require" accept='image/*' value="<?php echo $profilepic; ?>">
                  <p>Please Attach 2x2 Picture</p>
                </div>
              </div>
              <!-- End Profile Picture -->
              <!-- Registraion Form -->
              <div class="col-md-9">
								<div class="card-header">
                  <label for="">Personal Information</label>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">First Name</label>
                      <!-- <input type="text" class="form-control" name="fname" placeholder="first name" required="require" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)" value="<?php echo $fname; ?>" pattern="[A-Za-z]" title="First name"> -->
                      <input type="text" class="form-control" name="fname" placeholder="first name" required="require" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)" value="<?php echo $fname; ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Middle Name</label>
                      <input type="text" class="form-control" name="mname" placeholder="middle name" required="require" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)" value="<?php echo $mname; ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Last Name</label>
                      <input type="text" class="form-control" name="lname" placeholder="last name" required="require" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)" value="<?php echo $lname; ?>">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="">Gender</label>
                      <div class="">
                        <input type="radio" name="Gender" value="Male" required="require" /> Male                                                                                     
                      	<input type="radio" name="Gender" value="Female" required="require" />Female
                    	</div>
                    </div>
                  </div>
									
									<div class="col-md-3">
                    <div class="form-group">
                      <label for="">Birthday</label>
                      <input type="date" class="form-control" name="Bdate" required="require" value="<?php echo $Bdate; ?>">
                    </div>
                  </div>
									<div class="col-md-12">
                    <div class="card-header">
                      <label for="">Contact Information</label>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="">Phone Number</label>
                        <input type="text" class="form-control" name="mnumber" placeholder="Phone Number" required="require" onkeypress='return event.charCode >= 48 && event.charCode <= 57' minimumlength="11" maxlength="11" pattern="^(09)\d{9}$" title="Start at 09 and follow by 9 other numbers" value="<?php echo $mnumber; ?>">
                      </div>
                    </div>
										<div class="col-md-6">
                      <div class="form-group">
                      	<label for="">Email</label>
                        <input type="email" class="form-control" name="Email" placeholder="Email" required="require" pattern=".+@[g][m][a][i][l][.][c][o][m]" title="Only @gmail.com accounts are allowed " value="<?php echo $Email; ?>">
                      </div>
                    </div>
									</div>
                  <div class="col-md-12">
                    <div class="card-header">
                      <label for="">Address</label>
                    </div>
                  	<div class="row select-brgy">
                      <div class="col-md-3">
                        <div class="form-group">
                          <!-- <input type="text" class="form-control" name="address" placeholder="Barangay" required="require" value="<?php echo $address; ?>"> -->
                          <select id="list" name="address" onchange="getSelectValue();" required="require">
                                <option value="" disabled selected>Select Baranggay</option>
                                <option value="Bagsangan">Bagsangan</option>
                                <option value="Bacolod (Poblacion)">Bacolod (Poblacion)</option>
                                <option value="Batang">Batang</option>
                                <option value="Bolos">Bolos</option>
                                <option value="Buenavista">Buenavista</option>
                                <option value="Bulawan">Bulawan</option>
                                <option value="Carriedo">Carriedo</option>
                                <option value="Casini">Casini</option>
                                <option value="Cawayan">Cawayan</option>
                                <option value="Cogon">Cogon</option>
                                <option value="Gabao">Gabao</option>
                                <option value="Gulang-Gulang">Gulang-Gulang</option>
                                <option value="Santo Domingo (Lamboon)">Santo Domingo (Lamboon)</option>
                                <option value="Liang">Liang</option>
                                <option value="Mapaso">Mapaso</option>
                                <option value="Monbon">Monbon</option>
                                <option value="Patag">Patag</option>
                                <option value="Salvacion">Salvacion</option>
                                <option value="San Agustin (Poblacion)">San Agustin (Poblacion)</option>
                                <option value="San Isidro (Palogtok">San Isidro (Palogtok</option>
                                <option value="San Juan (Poblacion)">San Juan (Poblacion)</option>
                                <option value="San Julian (Poblacion)">San Julian (Poblacion)</option>
                                <option value="San Pedro (Poblacion)">San Pedro (Poblacion)</option>
                                <option value="Tabon-Tabon">Tabon-Tabon</option>
                                <option value="Tinampo">Tinampo</option>
                                <option value="Tongdol">Tongdol</option>
                            </select>
                            
                            <script>
                            
                            function getSelectValue()
                            {
                                var selectedValue = document.getElementById("list").value;
                                console.log(selectedValue);
                            }
                            getSelectValue();

                            </script>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" class="form-control" name="city" placeholder="Irosin" required="require" value="<?php echo $city; ?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" class="form-control" name="province" placeholder="Sorsogon" required="require" value="<?php echo $province; ?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" class="form-control" name="zipcode" placeholder="4707" required="require" value="<?php echo $zipcode; ?>" readonly>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="card-header">
                      <span class="fa fa-key"> Valid ID <small>(National ID, Brgy ID, Student ID)</small></span>
                    </div>
                  	<div class="form-group">
                    	<div class="input-group">
                      	<div class="custom-file">
                        	<input type="file" class="custom-file-input" name="validID" id="exampleInputFile" value="<?php echo $validID; ?>" accept="image/png, image/jpg, image/jpeg" required="require">
                        	<label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      	</div>
                    	</div>
                  	</div>
                	</div>
									<!-- Hide -->
                  <div class="from-group" style="display: none">
                    <label>
                      <input type="radio" name="education" value="Please add infromation" checked /> Please add infromation
                      <input type="radio" name="Age" value="none" checked /> 
                    </label>
                    <label>
                      <input type="radio" name="skill" value="Please add infromation" checked /> Please add infromation
                    </label>
                    <label>
                      <input type="radio" name="experience" value="Please add infromation" checked /> Please add infromation
                    </label>
                    <label>
                      <input type="radio" name="proTitle" value="Please add infromation" checked /> Please add infromation
                    </label>
                    <label>
                      <input type="radio" name="NC" value="Please add infromation" checked /> Please add infromation
                    </label>
                  </div>
									<!-- end hide -->
									<div class="col-md-12">
                    <div class="card-header">
                      <span>Account</span>
                    </div>
                    <div style="color:red;">
                    <p style="color:red;"><?php echo $errorMsg2; ?></p>
                </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" class="form-control" name="username" placeholder="username" onkeypress="return (event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode >= 48 && event.charCode <= 57)" value="<?php echo $username; ?>" maxlength="16" required="require">
                        <small>Max length 16, accept only alpha numerics, no special characters.</small>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" class="form-control" name="password" placeholder="password" onkeypress="return (event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode >= 48 && event.charCode <= 57)" value="<?php echo $password; ?>" maxlength="16" required="require">
                        <small>Max length 16, accept only alpha numerics, no special characters.</small>
                        <!-- <i class="fas fa-check-circle"></i>
			                  <i class="fas fa-exclamation-circle"></i> -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                        <!-- Agree to Terms and Conditions -->
                        <input type="checkbox" name="remember" value="true" required="require"/> I agree to the Terms of Service
                        <!-- <i class="fas fa-check-circle"></i>
			                  <i class="fas fa-exclamation-circle"></i> -->
                      </div>
                  </div>
               

									<!-- Hide the radio button-->
                  <div class="form-group " style="display: none">
                    <label class="col-sm-4 control-label">Usertype</label>
                    <div class="col-sm-5">
                      <div class="radio">
                        <label>
                          <input type="radio" name="usertype" value="employee" checked/> Employee
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="usertype" value="employer"  /> Employer
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                    	<!-- Do NOT use name="submit" or id="submit" for the Submit button -->
                      <button type="submit" name="register" class="btn ml-auto rounded-pill btn-register">Register</button>
                    </div>
										<p>Already have an account? <a href="elogin.php">Login here</a>.</p>
                  </div>
									<!--End Hide radio button-->
                </div>
              </div>
              <!-- End Registraion Form -->
            </div>
          </div>
        </form>
      </div>
      <!-- end card -->
    </div>
  </section>
</div>
<!-- End Registration card -->
<script>
  function triggerClick(e) {
  	document.querySelector('#profileImage').click();
  }
  function displayImage(e) {
    if (e.files[0]) {
      var reader = new FileReader();
      	reader.onload = function(e){
      	document.querySelector('#profileDisplay').setAttribute('src', e.target.result);
    	}
      reader.readAsDataURL(e.files[0]);
  	}
  }
</script>
    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/theme.js"></script>
</body>
</html>