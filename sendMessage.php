<?php include('server.php');
$fletter="";
if(isset($_SESSION["Username"])){
    $username=$_SESSION["Username"];
    if ($_SESSION["Usertype"]==1) {
		$linkPro="employeeProfile.php";
		$linkEditPro="editEmployee1.php";
		$linkBtn="applyJob.php";
		$textBtn="Apply for this job";
	}
	else{
		$linkPro="employerProfile.php";
		$linkEditPro="editEmployer1.php";
		$linkBtn="editJob.php";
		$textBtn="Edit the job offer";
	}
}
else{
    $username="";
    //header("location: index.php");
}

if(isset($_SESSION["msgRcv"])){
    $msgRcv=$_SESSION["msgRcv"];
}

if(isset($_POST["send"])){
    $msgTo=$_POST["msgTo"];
    $msgBody=$_POST["msgBody"];
    $Toname=$_POST["Toname"];
    $Sndr=$_POST["Sndr"];

    $sql = "INSERT INTO message (sender, receiver, msg, Toname, Sndr) VALUES ('$username', '$msgTo', '$msgBody', '$Toname', '$Sndr')";
    $result = $conn->query($sql);
    if($result==true){
        header("location: message.php");
    }
}






 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Send Message</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" type="text/css" href="dist/css/bootstrap.min.css">
	  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="asset/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="asset/css/mystyle.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500;600;700;800&display=swap" rel="stylesheet">   
<style>
	 body {
        padding-top: 3%;
        margin: 0;
        font-family: 'Kanit', sans-serif;
     }
  .gradient {
        background: linear-gradient( 
        120deg,#343a40,#6299a4);
        color: #fff;
      }
      .portfolio-navbar .navbar-nav .nav-link {
        font-weight: 600;
        font-size: 20px;
        padding: 2rem .1rem;
    }
	.card{box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); background:#fff}
    form.search {
  padding-top: 25px;
}
    form.search input[type=text] {
  padding: 10px;
  font-size: 17px;
  border: 1px solid #2d3436;
  float: left;
  width: 80%;
  background: #ffffff;
  color: #ADADAD;
}  
form.search button {
  float: left;
    width: 15%;
    padding: 10px;
    background: #2d3436;
    color: white;
    font-size: 17px;
    border: 1px solid #2d3436;
    border-left: none;
}
    
    #count1{
  border-radius: 50%;
  position: relative;
  top: -10px;
  left: -10px;

}
#count{
  border-radius: 50%;
  position: relative;
  top: -10px;
  left: -10px;

}
</style>

</head>
<body>

<!--Navbar menu--><!--Navbar menu-->
<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-light portfolio-navbar gradient py-0" id="nav">
        <div class="container"><a class="navbar-brand logo" href="<?php echo $linkPro; ?>"><img src="image/logo.png" height="75" alt="Logo"></a><button data-toggle="collapse" class="navbar-toggler" data-target="#navbarNav"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse"id="navbarNav">
                <ul class="nav navbar-nav ml-auto">
              
                  <form class="search" action="allJob.php" method="post">
				            <div class="form-group search">
				              <input type="text" name="s_title" placeholder="Search">
				              <button type="submit" class="search-icon"><i class="fa fa-search"></i></button>
				            </div>
	                </form>
               
                  <li class="nav-item" role="presentation"><a class="nav-link active" href="allJob.php">Offer Services</a></li>
                  <?php 
                  $sql_get = mysqli_query($conn,"SELECT * FROM message WHERE receiver='$username' and status=0");
                  $count = mysqli_num_rows($sql_get);

                  ?>
      <li class="nav-item" role="presentation"><a class="nav-link" href="message.php"><i class="fas fa-comments fa-lg"> </i> <span class="badge bg-primary" id="count"><?php echo $count; ?></span></a> </li>

      <?php
            if($_SESSION["Usertype"]==1){
              $sql_get = mysqli_query($conn,"SELECT * FROM selected WHERE f_username='$username' AND valid=1 AND statss=1");
              $count1 = mysqli_num_rows($sql_get);
              echo  '<li class="nav-item" role="presentation"><a class="nav-link" href="#" data-toggle="modal" data-target="#notification"><i class="fas fa-bell"></i> <span class="badge bg-primary" id="count1">'.$count1.'</span></a> </li>';
            } else {
              $sql_get = mysqli_query($conn,"SELECT * FROM apply WHERE fuser='$username'");
              $count1 = mysqli_num_rows($sql_get);
              echo '<li class="nav-item" role="presentation"><a class="nav-link" href="notif.php"><i class="fas fa-bell"></i> <span class="badge bg-primary" id="count1">'.$count1.'</span></a> </li>';
            }
      ?>
                  <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  <i class="fas fa-user-circle fa-lg"></i>
                  </a>
                  <ul class="dropdown-menu dropdown" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="<?php echo $linkPro; ?>">Profile</a></li>
                    <li><a class="dropdown-item" href="<?php echo $linkEditPro; ?>">Edit Profile</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="logout.php">Logout</a></li>
                  </ul>
                  </li>
                </ul>
            </div>
        </div>
    </nav>    
<!--End Navbar menu-->


<div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="page-header">
                    <h2>Write Message</h2>
                </div>

                <form id="registrationForm" method="post" class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-4 control-label">To</label>
                    <div class="col-sm-8">
                    <?php
                    if ($_SESSION["Usertype"]==1)
                    {
                      $sql = "SELECT * FROM employe WHERE username='$username' ";
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                              $username=$row["username"];
                              $password=$row["password"];
                              $profilepic=$row["profilepic"];
                              $fname=$row["fname"];
                              $mname=$row["mname"];
                              $lname=$row["lname"];
                            
                              $date = date("M d, Y",strtotime($Bdate));
                              $fn = $fname;
                              $mn = $mname;
                              $ln = $lname;
                              $fletter = $mn [0];
                              }
                        }
                    }else{
                      $sql = "SELECT * FROM employer WHERE username='$username' ";
                      $result = $conn->query($sql);
                      if ($result->num_rows > 0) {
                          // output data of each row
                          while($row = $result->fetch_assoc()) {
                            $username=$row["username"];
                            $password=$row["password"];
                            $profilepic=$row["profilepic"];
                            $fname=$row["fname"];
                            $mname=$row["mname"];
                            $lname=$row["lname"];
                          $fn = $fname;
                          $mn = $mname;
                          $ln = $lname;
                            $date = date("M d, Y",strtotime($Bdate));
                      
                            $fletter = $mn [0];
                            }
                      }
                    }
                    ?>
                    <input type="text" class="form-control" name="Sndr" value="<?php echo $fn; ?> <?php echo $fletter; ?>. <?php echo $ln; ?>" style="display: none;"  readonly/>
                    
                      <?php
                        $sql = "SELECT * FROM employer WHERE username='$msgRcv' ";
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                              $username=$row["username"];
                              $password=$row["password"];
                              $profilepic=$row["profilepic"];
                              $fname=$row["fname"];
                              $mname=$row["mname"];
                              $lname=$row["lname"];
                            
                              $date = date("M d, Y",strtotime($Bdate));
                        
                              $fletter = $mname [0];
                              }
                        } else {
                            echo "0 results";
                        }
                      ?>
                       
                        <input type="text" class="form-control" name="Toname" value="<?php echo $fname; ?> <?php echo $fletter; ?>. <?php echo $lname; ?>" readonly/>
                        <p>Username:</p>
                        <input type="text" class="form-control" name="msgTo" value="<?php echo $msgRcv; ?>" readonly/>
                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Message Body</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" id="txtarea" rows="12" name="msgBody" maxlength="300" required="require"></textarea>
                        <p class="text-right" id="charNum">300 Left</p>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <!-- Do NOT use name="submit" or id="submit" for the Submit button -->
                        <button type="submit" name="send" class="btn btn-info btn-lg">Send Message</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>





    <!-- <script src="assets/js/jquery-3.4.1.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="sassets/js/jquery.min.js"></script> -->
<script src="assets/js/jquery-3.4.1.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.min.js"></script>

      <script src="asset/jquery/jquery.min.js"></script>
      <script src="asset/js/bootstrap.bundle.min.js"></script>
      <script src="asset/js/adminlte.js"></script>
      <!-- DataTables  & Plugins -->
      <script src="asset/tables/datatables/jquery.dataTables.min.js"></script>
      <script src="asset/tables/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
      <script src="asset/tables/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
      <script src="asset/tables/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script>
    var el;                                                    

function countCharacters(e) {                                    
  var textEntered, countRemaining, counter;          
  textEntered = document.getElementById('txtarea').value;  
  counter = (300 - (textEntered.length) + " left");
  countRemaining = document.getElementById('charNum'); 
  countRemaining.textContent = counter;       
}
el = document.getElementById('txtarea');                   
el.addEventListener('keyup', countCharacters, false);
</script>

</body>
</html>