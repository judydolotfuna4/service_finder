<?php include('server.php');
$fletter="";

if(isset($_SESSION["Username"])){
    $username=$_SESSION["Username"];
    if ($_SESSION["Usertype"]==1) {
		$linkPro="employeeProfile.php";
		$linkEditPro="editEmployee1.php";
		$linkBtn="applyJob.php";
		$textBtn="Apply for this job";
	}
	else{
		$linkPro="employerProfile.php";
		$linkEditPro="editEmployer1.php";
		$linkBtn="editJob.php";
		$textBtn="Edit the job offer";
	}
}
else{
    $username="";
}


if(isset($_SESSION["msgRcv"])){
    $msgRcv=$_SESSION["msgRcv"];
} 


if(isset($_POST["send"])){
  $msgTo=$_POST["msgTo"];
  $msgBody=$_POST["msgBody"];
  $Toname=$_POST["Toname"];
  $RRR=$_POST["RRR"];
    $sql = "INSERT INTO message (sender, receiver, msg, Toname, Sndr) VALUES ('$username', '$msgTo', '$msgBody', '$Toname', '$RRR')";
    $result = $conn->query($sql);
    if($result==true){
        header("location: message.php");
    }
}


    
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Send Message</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" type="text/css" href="dist/css/bootstrap.min.css">
	  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="asset/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="asset/css/mystyle.css">
	  <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500;600;700;800&display=swap" rel="stylesheet">
<style>
body {
        padding-top: 3%;
        margin: 0;
        font-family: 'Kanit', sans-serif;
    }
    .logo img {
      height: 60px;
    }
	.card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); 
        background:#fff;
    }
	.gradient {
        background: linear-gradient( 
        120deg,#343a40,#6299a4);
        color: #fff;
      }
	.portfolio-navbar .navbar-nav .nav-link {
        font-weight: 600;
        font-size: 20px;
        padding: 15px 0;
		color: #000;
    }
	form.search {
  padding-top: 10px;
}
form.search input[type=text] {
  padding: 5px;
    font-size: 17px;
    border: 1px solid #2d3436;
    float: left;
    width: 75%;
    background: #ffffff;
    color: #ADADAD;
}  
form.search button {
  float: left;
    width: 15%;
    padding: 5px;
    background: #2d3436;
    color: white;
    font-size: 17px;
    border: 1px solid #2d3436;
    border-left: none;
}
/* notification */
#count{
  border-radius: 50%;
  position: relative;
  top: -10px;
  left: -10px;

}
#count1{
  border-radius: 50%;
  position: relative;
  top: -10px;
  left: -10px;

}
.portfolio-navbar .navbar-nav .nav-item {
  padding-right: 10px;
}
</style>

</head>
<body>

<!--Navbar menu-->
<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-light portfolio-navbar gradient py-0" id="nav">
  <div class="container">
    <a class="navbar-brand logo" href="<?php echo $linkPro ?>"> <img src="image/logo.png" height="75" alt="Logo"></a><button data-toggle="collapse" class="navbar-toggler" data-target="#navbarNav"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
    <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="nav navbar-nav ml-auto">
      
      <li class="nav-item" role="presentation"><a class="nav-link" href="allJob.php">Offer Services</a></li>

      <?php 
$sql_get = mysqli_query($conn,"SELECT * FROM message WHERE receiver='$username' and status=0");
$count = mysqli_num_rows($sql_get);

?>
      <li class="nav-item" role="presentation"><a class="nav-link" href="message.php"><i class="fas fa-comments fa-lg"> </i> <span class="badge bg-primary" id="count"><?php echo $count; ?></span></a> </li>

	  <?php
            if($_SESSION["Usertype"]==1){
              $sql_get = mysqli_query($conn,"SELECT * FROM selected WHERE f_username='$username' AND valid=1 AND statss=1");
              $count1 = mysqli_num_rows($sql_get);
              echo  '<li class="nav-item" role="presentation"><a class="nav-link" href="#" data-toggle="modal" data-target="#notification"><i class="fas fa-bell"></i> <span class="badge bg-primary" id="count1">'.$count1.'</span></a> </li>';
            } else {
              $sql_get = mysqli_query($conn,"SELECT * FROM apply WHERE fuser='$username'");
              $count1 = mysqli_num_rows($sql_get);
              echo '<li class="nav-item" role="presentation"><a class="nav-link" href="notif.php"><i class="fas fa-bell"></i> <span class="badge bg-primary" id="count1">'.$count1.'</span></a> </li>';
            }
      ?>

      <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
      <i class="fas fa-user-circle fa-lg"></i>
      </a>
      <ul class="dropdown-menu dropdown" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="<?php echo $linkPro ?>">Profile</a></li>
        <li><a class="dropdown-item" href="<?php echo $linkEditPro ?>">Edit Profile</a></li>
        <li><hr class="dropdown-divider"></li>
        <li><a class="dropdown-item" href="logout.php">Logout</a></li>
      </ul>
      </li>

    </ul>
    </div>
  </div>
</nav>     
<!--End Navbar menu-->


<div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="page-header">
                    <br>
                    <br>
                </div>
                
                <?php
    $accountid = $_GET["id"];
    $connection = mysqli_connect("localhost", "root", "", "service_finder");
    $sql = "SELECT * FROM message WHERE id = $accountid";
    $result = mysqli_query($connection, $sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $id=$row["id"];
            $sender=$row["sender"];
            $receiver=$row["receiver"];
            $msg=$row["msg"];
            $status=$row["status"];
            $Sndr=$row["Sndr"];
            $timestamp=$row["timestamp"];

          if($sender == $sender)
          {
            $fllname = $Sndr;
          }
            
            }
    } else {
        echo "0 results";
    }
  


    
    
                ?>

                <form id="registrationForm" method="post" class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-4 control-label">From</label>
                    <div class="col-sm-8">
                   
                    <input type="text" class="form-control" name="Toname" value="<?php echo $fllname; ?>" readonly/>
                    
                    <p>Username: </p>
                        <input type="text" class="form-control" name="msgTo" value="<?php echo $sender; ?>" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Message</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" rows="5" placeholder="<?php echo $msg; ?>" readonly ></textarea>
                    </div>
                </div>
                

                <div class="form-group">
                    <label class="col-sm-4 control-label">Message Body</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" rows="6" name="msgBody" id="txtarea" maxlength="300" title="You reach the maximum message" required="require" ></textarea>
                        <p class="text-right" id="charNum">300 Left</p>
                    </div>
                    
                </div>
                <?php
                    if ($_SESSION["Usertype"]==1)
                    {
                      $sql = "SELECT * FROM employe WHERE username='$username' ";
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                              $username=$row["username"];
                              $password=$row["password"];
                              $profilepic=$row["profilepic"];
                              $fname=$row["fname"];
                              $mname=$row["mname"];
                              $lname=$row["lname"];
                            
                              $date = date("M d, Y",strtotime($Bdate));
                              $fn = $fname;
                              $mn = $mname;
                              $ln = $lname;
                              $fletter = $mn [0];
                              }
                        }
                    }else{
                      $sql = "SELECT * FROM employer WHERE username='$username' ";
                      $result = $conn->query($sql);
                      if ($result->num_rows > 0) {
                          // output data of each row
                          while($row = $result->fetch_assoc()) {
                            $username=$row["username"];
                            $password=$row["password"];
                            $profilepic=$row["profilepic"];
                            $fname=$row["fname"];
                            $mname=$row["mname"];
                            $lname=$row["lname"];
                          $fn = $fname;
                          $mn = $mname;
                          $ln = $lname;
                            $date = date("M d, Y",strtotime($Bdate));
                      
                            $fletter = $mn [0];
                            }
                      }
                    }
                    ?>
                    <input type="text" class="form-control" name="RRR" style="display: none" value="<?php echo $fn; ?> <?php echo $fletter; ?>. <?php echo $ln; ?>"  readonly/>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <!-- Do NOT use name="submit" or id="submit" for the Submit button -->
                        <button type="submit" name="send" class="btn btn-info btn-lg">Send Message</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <div id="notification" class="modal animated rubberBand delete-modal" role="dialog">
         <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
               <div class="modal-body text-center">
                  <form>
                     <div class="card-body">
                        <div class="row">
                        <?php 
                          $sql_get = mysqli_query($conn,"SELECT * FROM selected WHERE f_username='$username' and valid=1");
                          $count1 = mysqli_num_rows($sql_get);

                        ?>
                           <div class="col-md-12">
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group">
                <table style="width:100%">
                      <tr>
                          <th>Message</th>
                          <!-- <th>Action</th>
                          <th>hello</th> -->
                          
                      </tr>
                      <?php 
                      		$sql = "SELECT * FROM job_offer,selected WHERE job_offer.job_id=selected.job_id AND selected.f_username='$username' AND selected.valid=1 ORDER BY job_offer.deadline DESC";
                          $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                $job_id=$row["job_id"];
                                $title=$row["title"];
                                $e_username=$row["e_username"];
                                $statss=$row["statss"];
                               ?>
                                
                                  <tr>
                                    <!-- <td> <h5>Your are accepted by user: <?php echo $row['e_username']; ?> on Job title: <?php echo $row['title']; ?> </h5></td>
                                    <form action="employeeProfile.php" method="POST"> -->
                                    <!-- <input type="text" name="num2" value="<?php echo $row['job_id']; ?>">
                                    <input type="text" name="num1" value="0"> -->

                                    <!-- <td><a  class="btn-sm btn-primary" name="notifbtn" target="_blank"><i class="fas fa-times"></i></a></td> -->
                                    <!-- <td><button type="submit" name="notifbtn" class="btn btn-info btn-lg"><i class="fas fa-times"></i></button></td> -->

                                  
                                  <?php
                                 echo ' <td> <h5>Your are accepted by user: '.$e_username.' on Job title: '.$title.' </h5></td>
                                </tr>';
                                            }
                                    } else {
                                        echo "<tr><td>Nothing to show</td></tr>";
                                    }

                                  ?>
                              </table>
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer" style="border: none; background-color: white">
                        <a href="#" class="btn btn-danger" data-dismiss="modal">Close</a>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>



      <script src="assets/js/jquery-3.4.1.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.min.js"></script>

      <script src="asset/jquery/jquery.min.js"></script>
      <script src="asset/js/bootstrap.bundle.min.js"></script>
      <script src="asset/js/adminlte.js"></script>
      <!-- DataTables  & Plugins -->
      <script src="asset/tables/datatables/jquery.dataTables.min.js"></script>
      <script src="asset/tables/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
      <script src="asset/tables/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
      <script src="asset/tables/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script>
    var el;                                                    

function countCharacters(e) {                                    
  var textEntered, countRemaining, counter;          
  textEntered = document.getElementById('txtarea').value;  
  counter = (300 - (textEntered.length) + " left");
  countRemaining = document.getElementById('charNum'); 
  countRemaining.textContent = counter;       
}
el = document.getElementById('txtarea');                   
el.addEventListener('keyup', countCharacters, false);
</script>


</body>
</html>