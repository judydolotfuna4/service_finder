<?php 
$connect = new PDO("mysql:host=localhost;dbname=service_finder", "root", "");


$sql = "SELECT * FROM job_offer WHERE valid=1 and stat='Show'"; //ORDER BY timestamp DESC
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        $job_id = $row["job_id"];
        $title = $row["title"];
        $type = $row["type"];
        $picture = $row["picture"];
        $budget = $row["budget"];
        $f_name = $row["f_name"];
        $m_name = $row["m_name"];
        $l_name = $row["l_name"];
        $e_username = $row["e_username"];

        $ratingID = $job_id;

        
    }
} else {
    echo "<tr></tr><tr><td></td><td>Nothing to show</td></tr>";
}





if(isset($_POST["action"]))
{
	$average_rating = 0;
	$total_review = 0;
	$five_star_review = 0;
	$four_star_review = 0;
	$three_star_review = 0;
	$two_star_review = 0;
	$one_star_review = 0;
	$total_user_rating = 0;
	$review_content = array();

	$accountid = $_GET["job_id"];
	// $query = "SELECT review_table.user_rating, job_offer.e_username
	// FROM job_offer
	// LEFT JOIN review_table ON review_table.rjob_id = job_offer.job_id
	// ORDER BY review_table.user_rating";
	 $query = "SELECT  FROM review_table WHERE rjob_id=$accountid";

	$result = $connect->query($query, PDO::FETCH_ASSOC);

	foreach($result as $row)
	{
		$review_content[] = array(
			'user_name'		=>	$row["user_name"],
			'user_review'	=>	$row["user_review"],
			'rating'		=>	$row["user_rating"],
			'datetime'		=>	date('l jS, F Y h:i:s A', $row["datetime"])
		);

		if($row["user_rating"] == '5')
		{
			$five_star_review++;
		}

		if($row["user_rating"] == '4')
		{
			$four_star_review++;
		}

		if($row["user_rating"] == '3')
		{
			$three_star_review++;
		}

		if($row["user_rating"] == '2')
		{
			$two_star_review++;
		}

		if($row["user_rating"] == '1')
		{
			$one_star_review++;
		}

		$total_review++;

		$total_user_rating = $total_user_rating + $row["user_rating"];

	}

	$average_rating = $total_user_rating / $total_review;

	$output = array(
		'average_rating'	=>	number_format($average_rating, 1),
		'total_review'		=>	$total_review,
		'five_star_review'	=>	$five_star_review,
		'four_star_review'	=>	$four_star_review,
		'three_star_review'	=>	$three_star_review,
		'two_star_review'	=>	$two_star_review,
		'one_star_review'	=>	$one_star_review,
		'review_data'		=>	$review_content
	);

	echo json_encode($output);

 }
