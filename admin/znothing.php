<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <!-- ./wrapper -->

   <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
         <div class="container-fluid">
            <div class="row mb-2">
               <div class="col-sm-6">
                  <h1 class="m-0"><i class="fa fa-users"></i> Service Provider</h1>
               </div>
               <!-- /.col -->
            </div>
         </div>
      </div>
      <section class="content">
         <div class="container-fluid">
            <div class="card card-info">
               <br>
               <div class="col-md-12">
                  <table id="example2" class="table table-bordered">
                     <thead style="background-color: rgb(48, 247, 187);">
                        <tr>
                           <th>Profile</th>
                           <th>Full Name</th>
                           <th>Contact</th>
                           <th>Email</th>
                           <th>Access Level</th>
                           <th>Account</th>
                           <th>Password</th>
                           <th>Valid ID</th>
                           <th>Certificate</th>
                           <th class="text-center">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                        $sql = "SELECT * FROM employer";
                        $connection = mysqli_connect("localhost", "root", "", "service_finder");
                        $result = mysqli_query($connection, $sql);

                        ?>
                        <?php
                        if ($result->num_rows > 0) {
                           // output data of each row
                           while ($row = $result->fetch_assoc()) {
                              $fname = $row["fname"];
                              $mname = $row["mname"];
                              $lname = $row["lname"];
                              $Email = $row["Email"];
                              $password = $row["password"];
                              $profilepic = $row["profilepic"];
                              $username = $row["username"];
                              $Bdate = $row["Bdate"];
                              $validID = $row["validID"];
                              $NC = $row["NC"];

                              echo '
                                <form action="allJob.php" method="post">
                                <input type="hidden" name="jid" value="' . $username . '">
                                    <tr>
                                    <td><img src="../asset/img/' . $profilepic . '" width="100" style="border: 2px solid #ddd"></td>
                                    <td>' . $lname . ' ' . $fname . '</td>
                                    <td>' . $mname . '</td>
                                    <td>' . $Bdate . '</td>
                                    <td>' . $Email . '</td>
                                    <td>' . $username . '</td>
                                    <td type="password" >' . $password . '</td>
                                    <td>' . $validID . '</td>
                                    <td>' . $NC . '</td>
                                    <td type="text-center">
                                    <a class="btn btn-sm btn-success" href="#" data-toggle="modal"
                                       data-target="#edit"><i
                                       class="fa fa-user-edit"></i> Update</a>
                                    <a class="btn btn-sm btn-danger" href="" name="' . $username . '" data-toggle="modal"
                                       data-target="#delete"><i
                                       class="fa fa-trash"></i> Delete</a>
                                    </td>
                                    </tr>
                                </form>
                                ';
                           }
                        } else {
                           echo "<tr></tr><tr><td></td><td>Nothing to show</td></tr>";
                        }

                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </section>
   </div>
</body>
</html>