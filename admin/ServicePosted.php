<?php
// include('../server.php');
include "adminServer.php";
if (isset($_SESSION["Username"])) {
} else {
   $username = "";
   header("location: adminlogin.php");
}


// update
if (isset($_POST["update_submit"])) {
   $username = $_GET["username"];
   $username = $_POST["username"];
   $password = $_POST["password"];
   $connection = mysqli_connect("localhost", "root", "", "service_finder");

   $sql = "UPDATE employe SET username = '$username', password = '$password' WHERE id = $accountid ";

   if (mysqli_query($connection, $sql)) {
      $success_update = true;
   }
}

// delete
if (isset($_POST["delete_submit"])) {
   $username = $_GET["username"];
   $connection = mysqli_connect("localhost", "root", "", "service_finder");

   $sql = "DELETE FROM employe WHERE username = $username";

   if (mysqli_query($connection, $sql)) {
      $success_delete = true;
   }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Admin User Accounts</title>
   <!-- Font Awesome -->
   <link rel="stylesheet" href="../asset/fontawesome/css/all.min.css">
   <link rel="stylesheet" href="../asset/css/adminlte.min.css">
   <link rel="stylesheet" href="../asset/css/style.css">
   <link rel="stylesheet" href="../asset/tables/datatables-bs4/css/dataTables.bootstrap4.min.css">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500;600;700;800&display=swap" rel="stylesheet">
   <style type="text/css">
      body {
         margin: 0;
         font-family: 'Kanit', sans-serif;
      }

      .gradient {
         background: linear-gradient(120deg, #343a40, #6299a4);
         color: #fff;
      }

      table tr td {
         padding: 0.3rem !important;
      }

      table tr td p {
         margin-top: -0.8rem !important;
         margin-bottom: -0.8rem !important;
         font-size: 0.9rem;
      }

      td a.btn {
         font-size: 0.7rem;
      }

      .table thead {
         background: linear-gradient(120deg, #343a40, #6299a4);
         color: #fff;
         text-align: center;
      }

      .main-sidebar {
         background: linear-gradient(180deg, #343a40, #6299a4);
      }

      nav.mt-2 ul.nav-sidebar li p,
      .wrapper i {
         color: #fff;
         font-weight: 600;
      }
   </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
   <!-- wrapper -->
   <div class="wrapper">
      <nav class="main-header navbar navbar-expand gradient">
         <ul class="navbar-nav">
            <li class="nav-item">
               <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
         </ul>

         <ul class="navbar-nav">
            <li class="nav-item">
               <a class="nav-link" data-widget="pushmenu" href="user.php" role="button"><i class="">Home</i></a>
            </li>
         </ul>

         <ul class="navbar-nav ml-auto">
            <li class="nav-item">
               <a class="nav-link" href="logout.php">
                  <i class="fas fa-sign-out-alt"></i>
               </a>
            </li>
         </ul>
      </nav>
   </div>
   <!--end wrapper -->
   <!--Aside -->
   <aside class="main-sidebar sidebar-light-primary">
      <!-- Brand Logo -->
      <a href="index.html" class="brand-link">
         <img src="../image/logo.png" alt="Logo" width="200">
      </a>
      <div class="sidebar">
         <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">


               <li class="nav-item">
                  <a href="user.php" class="nav-link">
                     <i class="fa fa-users"></i>
                     <p>
                        Customer
                     </p>
                  </a>
               </li>
               <li class="nav-item">
                  <a href="ServiceProvider.php" class="nav-link">
                     <i class="fa fa-hand-holding-heart"></i>
                     <p>
                        Service Provider
                     </p>
                  </a>
               </li>
               <li class="nav-item">
                  <a href="ServicePosted.php" class="nav-link">
                     <i class="fa fa-handshake"></i>
                     <p>
                        Service Posted
                     </p>
                  </a>
               </li>

               <li class="nav-item">
                  <a href="employee.php" class="nav-link">
                     <i class="fas fa-user-tie"></i>
                     <p>
                        Employee
                     </p>
                  </a>
               </li>

               <li class="nav-item">
                  <a href="employer.php" class="nav-link">
                     <i class="fas fa-user-tie"></i>
                     <p>
                        Employer
                     </p>
                  </a>
               </li>

               <!-- <li class="nav-item">
                     <a href="#" class="nav-link">
                        <i class="fa fa-chart-bar"></i>
                        <p>
                           Reports
                        </p>
                        <i class="right fas fa-angle-left"></i>
                     </a>
                     <ul class="nav nav-treeview">
                        
                        <li class="nav-item">
                           <a href="jobs-report.html" class="nav-link">
                              <i class="nav-icon far fa-circle"></i>
                              <p>Jobs</p>
                           </a>
                        </li>
                        <li class="nav-item">
                           <a href="job-completed-report.html" class="nav-link">
                              <i class="nav-icon far fa-circle"></i>
                              <p>Jobs Completed</p>
                           </a>
                        </li>
                     </ul>
                  </li> -->

            </ul>
         </nav>
      </div>
   </aside>

   <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
         <div class="container-fluid">
            <div class="row mb-2">
               <div class="col-sm-6">
                  <h1 class="m-0"><i class="fa fa-handshake"></i> Service Posted</h1>
               </div>
               <!-- /.col -->

            </div>
         </div>
      </div>
      <section class="content">
         <div class="container-fluid">
            <div class="card card-info">
               <br>
               <div class="col-md-12">
                  <table id="example2" class="table table-bordered">
                     <thead style="background-color: rgb(48, 247, 187);">
                        <tr>
                           <td>Name</td>
                           <td>Title</td>
                           <td>Type</td>
                           <td>Budget</td>
                           <td>Employer</td>


                        </tr>
                     </thead>
                     <tbody>
                        <?php
                        $sql = "SELECT * FROM job_offer";
                        $connection = mysqli_connect("localhost", "root", "", "service_finder");
                        $result = mysqli_query($connection, $sql);

                        ?>
                        <?php
                        if ($result->num_rows > 0) {
                           // output data of each row
                           while ($row = $result->fetch_assoc()) {
                              $job_id = $row["job_id"];
                              $title = $row["title"];
                              $type = $row["type"];
                              $budget = $row["budget"];
                              $f_name = $row["f_name"];
                              $m_name = $row["m_name"];
                              $l_name = $row["l_name"];
                              $e_username = $row["e_username"];


                              echo '
                                     <form action="allJob.php" method="post">
                                     <input type="hidden" name="jid" value="' . $job_id . '">
                                         <tr>
                                         <td>' . $f_name . ' ' . $m_name . '. ' . $l_name . '</td>
                                         <td>' . $title . '</td>
                                         <td>' . $type . '</td>
                                         <td>' . $budget . '</td>
                                         <td>' . $e_username . '</td>
                                         
                                         </tr>
                                     </form>
                                     ';
                           }
                        } else {
                           echo "<tr>
                                             <td>Nothing to show</td>
                                             <td>Nothing to show</td>
                                             <td>Nothing to show</td>
                                             <td>Nothing to show</td>
                                             <td>Nothing to show</td>
                                             <td>Nothing to show</td>
                                             <td>Nothing to show</td>
                                             <td>Nothing to show</td>
                                             <td>Nothing to show</td>
                                             <td>Nothing to show</td>
                                             </tr>";
                        }

                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </section>
   </div>
   <!-- /.content-wrapper -->
   </div>
   
   <!-- /.content-wrapper -->
   </div>
   <!-- ./wrapper -->

   <!-- jQuery -->
   <script src="../asset/jquery/jquery.min.js"></script>
   <script src="../asset/js/bootstrap.bundle.min.js"></script>
   <script src="../asset/js/adminlte.js"></script>
   <!-- DataTables  & Plugins -->
   <script src="../asset/tables/datatables/jquery.dataTables.min.js"></script>
   <script src="../asset/tables/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
   <script src="../asset/tables/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
   <script src="../asset/tables/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
</body>

</html>