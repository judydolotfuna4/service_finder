<?php include('server.php');
if(isset($_SESSION["Username"])){
	$username=$_SESSION["Username"];
}
else{
    $username="";
	//header("location: index.php");
}

if(isset($_SESSION["job_id"])){
    $job_id=$_SESSION["job_id"];
}
else{
    $job_id="";
    //header("location: index.php");
}


$sql = "SELECT * FROM job_offer WHERE job_id='$job_id'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $title=$row["title"];
        $type=$row["type"];
        $description=$row["description"];
        $budget=$row["budget"];
        
        }
} else {
    echo "0 results";
}


if(isset($_POST["editJob"])){
    $Cert=test_input($_POST["Cert"]);
    $title=test_input($_POST["title"]);
    $type=test_input($_POST["type"]);
    $description=test_input($_POST["description"]);
    $budget=test_input($_POST["budget"]);
    


    $sql = "UPDATE job_offer SET title='$title',type='$type',description='$description', budget='$budget', Cert='$Cert' WHERE job_id='$job_id'";

    
    $result = $conn->query($sql);
    if($result==true){
        header("location: jobDetails.php");
    }
}


 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Edit Job Offer</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="asset/fontawesome/css/all.min.css">

<style>
	body{padding-top: 3%;margin: 0;
    color: #2d3436;}
	.card{box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); background:#fff}
    .container{border-width: 2px;}
    .card{width: 100rem;}
</style>

</head>
<body>



<div class="post-job" style="width: 30%;margin-left: 20%;margin-top: 50px;margin-bottom: 20px;">
<div class="card" style="width: 50rem">
<div class="container" style="margin: 10px 5% 4px 30%">
        <div class="row">
            <div class="col-md-9" >
                <div class="page-header">
                <div class="X" >
                    <li class="nav" style="float: right; padding-right:50px"><a href="jobDetails.php"><span class="fa fa-times" style="color: black;"></span></a></li>
                </div>
                    <h2>Post A Job Offer</h2>
                </div>

                <form id="registrationForm" method="post" class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-4 control-label">Job Title</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="title" value="<?php echo $title; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Job Type</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="type" value="<?php echo $type; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Job Description</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="description" value="<?php echo $description; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Minimum Wage</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="budget" value="<?php echo $budget; ?>" />
                    </div>
                </div>
                

                
                <!-- //================== First letter ======================================== -->
               
                <!-- //=========================================================== -->


                <div class="form-group" style="display: none">
                    <label class="col-sm-4 control-label">Required Skills</label>
                    
                    <div class="radio">
                                   
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="special_skill" value="Special_skill"  checked/> Special_skill
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="deadline" value="1999-01-01"  checked/> 1999-01-01
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="fuser" value="<?php echo $username; ?>" required="require" checked/><?php echo $username; ?>
                                    </label>
                                </div>
                </div>
<!-- ==================================== Add this  ============================-->
<div class="form-group " style="display: none">  <!-- display: none -->
                                        <label class="col-sm-4 control-label"></label>
                                        <div class="col-sm-5">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="f_name" value="<?php echo $fname ?>"  checked /><?php echo $fname ?>
                                                    <!-- Hey! Hey! Hey! Hey! ^^^^^^^   Hey! Hey! Hey! Hey! -->
                                                </label>
                                            </div>
                                            <div class="radio">

                                                <label>
                                                    <input type="radio" name="m_name" value="<?php echo $fletter ?>" checked /> <?php echo $fletter ?>
                                                    <!-- Hey! Hey! Hey! Hey! ^^^^^^^   Hey! Hey! Hey! Hey! -->
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="l_name" value="<?php echo $lname ?>" checked /> <?php echo $lname ?>
                                                    <!-- Hey! Hey! Hey! Hey! ^^^^^^^   Hey! Hey! Hey! Hey! -->
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="picture" value="<?php echo $profilepic ?>" checked /> <?php echo $profilepic ?>
                                                     <!-- Hey! Hey! Hey! Hey! ^^^^^^^   Hey! Hey! Hey! Hey! -->
                                                </label>
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
<!-- ====================================end Add this ============================-->
            
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <!-- Do NOT use name="submit" or id="submit" for the Submit button -->
                        <button type="submit" name="editJob" class="btn ml-auto rounded-pill btn-font-size px-4" style="background: rgb(116, 156, 143);color:#fff;">Update</button>
                    </div>
                </div>

                 
                <div class="form-group">
                    <div class="col sm-9 col-sm-offset-3">
                
                        <select id="list" name="Cert" onchange="getSelectValue();">
                        
                                <option value="None" required="require" >Select Certificate</option>
                                <?php 
                                        $sql = "SELECT * FROM addcertificate WHERE c_username='$username'";
                                        $result = mysqli_query($conn, $sql);
                                        while ($row = mysqli_fetch_array($result)) {
                                        $title=$row["title"];
                                        $Certificate=$row["Certificate"];?>
                                            
                                <option value="<?php echo $row["title"]; ?>"><?php echo $row["title"]; ?></option>
                                
                                
                                <?php } ?>
                            </select>
                            
                        <script>
                            
                            function getSelectValue()
                            {
                                var selectedValue = document.getElementById("list").value;
                                console.log(selectedValue);
                            }
                            getSelectValue();

                        </script>
                      
                    </div>
                </div>
                
   

            </form>
            </div>
        </div>
    </div>
</div>
</div>


<script src="assets/js/jquery-3.4.1.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.min.js"></script>

      <script src="asset/jquery/jquery.min.js"></script>
      <script src="asset/js/bootstrap.bundle.min.js"></script>
      <script src="asset/js/adminlte.js"></script>
      <!-- DataTables  & Plugins -->
      <script src="asset/tables/datatables/jquery.dataTables.min.js"></script>
      <script src="asset/tables/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
      <script src="asset/tables/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
      <script src="asset/tables/datatables-buttons/js/buttons.bootstrap4.min.js"></script>



</body>
</html>