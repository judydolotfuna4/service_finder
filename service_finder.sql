-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 07, 2021 at 06:23 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `service_finder`
--

-- --------------------------------------------------------

--
-- Table structure for table `addcertificate`
--

DROP TABLE IF EXISTS `addcertificate`;
CREATE TABLE IF NOT EXISTS `addcertificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `Certificate` varchar(100) NOT NULL,
  `c_username` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addcertificate`
--

INSERT INTO `addcertificate` (`id`, `title`, `Certificate`, `c_username`) VALUES
(3, 'Web Build', 'valorant.png', 'nichol20'),
(4, 'Welder', 'NC.jpg', 'nichol20'),
(5, 'Web Build', 'thumb-1920-693742.png', 'john');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin01', '10nimda');

-- --------------------------------------------------------

--
-- Table structure for table `apply`
--

DROP TABLE IF EXISTS `apply`;
CREATE TABLE IF NOT EXISTS `apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_username` varchar(200) NOT NULL,
  `job_id` varchar(999) NOT NULL,
  `bid` varchar(1000) NOT NULL,
  `cover_letter` varchar(9999) NOT NULL,
  `fuser` varchar(20) DEFAULT NULL,
  `tItle` varchar(100) DEFAULT NULL,
  `tYpe` varchar(100) DEFAULT NULL,
  `NaMe` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employe`
--

DROP TABLE IF EXISTS `employe`;
CREATE TABLE IF NOT EXISTS `employe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `profilepic` varchar(200) NOT NULL DEFAULT 'None',
  `fname` varchar(20) NOT NULL,
  `mname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `Age` varchar(150) DEFAULT NULL,
  `Bdate` date NOT NULL,
  `mnumber` varchar(11) NOT NULL,
  `address` varchar(60) NOT NULL,
  `zipcode` varchar(1000) NOT NULL,
  `city` varchar(20) NOT NULL,
  `province` varchar(20) NOT NULL,
  `validID` varchar(200) NOT NULL,
  `NC` varchar(200) DEFAULT NULL,
  `skill` varchar(100) DEFAULT NULL,
  `education` varchar(200) DEFAULT NULL,
  `experience` varchar(500) DEFAULT NULL,
  `proTitle` varchar(300) DEFAULT NULL,
  `NCname` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employe`
--

INSERT INTO `employe` (`id`, `username`, `password`, `profilepic`, `fname`, `mname`, `lname`, `Email`, `Gender`, `Age`, `Bdate`, `mnumber`, `address`, `zipcode`, `city`, `province`, `validID`, `NC`, `skill`, `education`, `experience`, `proTitle`, `NCname`) VALUES
(1, 'nichol200', '123', 'man.png', 'Nichol', 'Reyes', 'Quezon', 'qmnichol@gmail.com', 'Male', '22', '2000-04-20', '09565844585', 'Zone 4', '4706', 'Bulan', 'Sorsogon', 'BARANGAYID6_1024x.jpg', 'valorant.png', 'Web Developer', 'Please add information', 'Please add information', 'Please add information', ''),
(41, 'ppikoy', '123', 'man.png', 'Justine', 'Delcastilio', 'Quezon', 'pikoy05@gmail.com', 'Male', NULL, '1995-02-05', '09632770945', 'Zone 2', '4706', 'Bulan', 'Sorsogon', 'BARANGAYID6_1024x.jpg', NULL, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(11) NOT NULL,
  `profilepic` varchar(200) NOT NULL DEFAULT 'None',
  `fname` varchar(20) NOT NULL,
  `mname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Gender` varchar(11) NOT NULL,
  `Age` varchar(150) DEFAULT NULL,
  `Bdate` date DEFAULT NULL,
  `mnumber` varchar(11) NOT NULL,
  `address` varchar(50) NOT NULL,
  `zipcode` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `validID` varchar(200) NOT NULL,
  `NC` varchar(50) DEFAULT NULL,
  `skill` varchar(50) DEFAULT NULL,
  `education` varchar(50) DEFAULT NULL,
  `experience` varchar(50) DEFAULT NULL,
  `proTitle` varchar(50) DEFAULT NULL,
  `NCname` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `username`, `password`, `profilepic`, `fname`, `mname`, `lname`, `Email`, `Gender`, `Age`, `Bdate`, `mnumber`, `address`, `zipcode`, `city`, `province`, `validID`, `NC`, `skill`, `education`, `experience`, `proTitle`, `NCname`) VALUES
(16, 'ppikoy', '123', 'man.png', 'Justine', 'Delcastilio', 'Quezon', 'pikoy05@gmail.com', 'Male', NULL, '1995-02-05', '09565844525', 'Zone 2', '4706', 'Bulan', 'Sorsogon', 'BARANGAYID6_1024x.jpg', 'Please add infromation', 'Please add infromation', 'Please add infromation', 'Please add infromation', 'Please add infromation', '');

-- --------------------------------------------------------

--
-- Table structure for table `employer`
--

DROP TABLE IF EXISTS `employer`;
CREATE TABLE IF NOT EXISTS `employer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `profilepic` varchar(200) NOT NULL DEFAULT 'None',
  `fname` varchar(20) NOT NULL,
  `mname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `Age` varchar(150) DEFAULT NULL,
  `Bdate` date NOT NULL,
  `mnumber` varchar(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `zipcode` varchar(9999) NOT NULL,
  `city` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL,
  `validID` varchar(200) NOT NULL,
  `NC` varchar(200) NOT NULL,
  `skill` varchar(100) DEFAULT NULL,
  `education` varchar(300) DEFAULT NULL,
  `experience` varchar(300) DEFAULT NULL,
  `proTitle` varchar(100) DEFAULT NULL,
  `NCname` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employer`
--

INSERT INTO `employer` (`id`, `username`, `password`, `profilepic`, `fname`, `mname`, `lname`, `Email`, `Gender`, `Age`, `Bdate`, `mnumber`, `address`, `zipcode`, `city`, `province`, `validID`, `NC`, `skill`, `education`, `experience`, `proTitle`, `NCname`) VALUES
(6, 'nichol20', '123', 'man1.png', 'Mark Nichol', 'Reyes', 'Quezon', 'qmnichol@gmail.com', 'Male', '21', '2000-03-20', '09565844585', 'Zone 2', '4706', 'Bulan', 'Sorsogon', 'valorant.png', 'Picture1.png', 'Web Developer', 'Please add infromation', 'Please add infromation', 'Please add infromation', ''),
(9, 'dave', '123', '075.webp', 'Hellow', 'a', 'a', 'qmnichol@gmail.com', 'Male', '22', '1999-04-05', '09565844185', 'Zone 2', '4706', 'Bulan', 'Sorsogon', '', '', 'Web Developer', 'Please add infromation', 'Please add infromation', 'Please add infromation', '');

-- --------------------------------------------------------

--
-- Table structure for table `employerr`
--

DROP TABLE IF EXISTS `employerr`;
CREATE TABLE IF NOT EXISTS `employerr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `profilepic` varchar(100) NOT NULL DEFAULT 'None',
  `fname` varchar(20) NOT NULL,
  `mname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `Age` varchar(100) NOT NULL,
  `Bdate` date NOT NULL,
  `mnumber` varchar(11) NOT NULL,
  `address` varchar(20) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `province` varchar(20) NOT NULL,
  `validID` varchar(100) NOT NULL,
  `NC` varchar(100) DEFAULT NULL,
  `skill` varchar(100) DEFAULT NULL,
  `education` varchar(100) DEFAULT NULL,
  `experience` varchar(100) DEFAULT NULL,
  `proTItle` varchar(100) DEFAULT NULL,
  `NCname` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employerr`
--

INSERT INTO `employerr` (`id`, `username`, `password`, `profilepic`, `fname`, `mname`, `lname`, `Email`, `Gender`, `Age`, `Bdate`, `mnumber`, `address`, `zipcode`, `city`, `province`, `validID`, `NC`, `skill`, `education`, `experience`, `proTItle`, `NCname`) VALUES
(2, 'employer1', '123', 'man2.png', 'daw', 'dawd', 'awd', 'qmnichol@gmail.com', 'Male', '', '2000-03-03', '09565844585', 'Select Barangay', '4706', 'Irosin', 'Sorsogon', 'BARANGAYID6_1024x.jpg', '', 'awd', 'Please add infromation', 'Please add infromation', 'Please add infromation', 'None');

-- --------------------------------------------------------

--
-- Table structure for table `job_offer`
--

DROP TABLE IF EXISTS `job_offer`;
CREATE TABLE IF NOT EXISTS `job_offer` (
  `job_id` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `description` varchar(300) NOT NULL,
  `budget` varchar(1000) NOT NULL,
  `e_username` varchar(20) NOT NULL,
  `valid` tinyint(1) NOT NULL,
  `f_name` varchar(20) NOT NULL,
  `m_name` varchar(20) NOT NULL,
  `l_name` varchar(20) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `skills` varchar(10) DEFAULT NULL,
  `special_skill` varchar(20) DEFAULT NULL,
  `deadline` varchar(20) DEFAULT NULL,
  `fuser` varchar(20) DEFAULT NULL,
  `stat` int(1) NOT NULL DEFAULT '1',
  `Cert` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_offer`
--

INSERT INTO `job_offer` (`job_id`, `title`, `type`, `description`, `budget`, `e_username`, `valid`, `f_name`, `m_name`, `l_name`, `picture`, `skills`, `special_skill`, `deadline`, `fuser`, `stat`, `Cert`) VALUES
(16, 'vvv', 'vvv', 'vvv', '20000', 'nichol20', 0, 'Mark Nichol', 'R', 'Quezon', 'Picture1.png', '', '', NULL, 'nichol20', 1, NULL),
(17, 'Web Build', 'Frontend', 'I am working on frontend for over 3 years', '10000', 'nichol20', 1, 'Mark Nichol', 'R', 'Quezon', 'Picture1.png', 'skills', 'Special_skill', '1999-01-01', 'nichol20', 1, NULL),
(38, 'asdf', 'asdf', 'asdf', '20000', 'dave', 1, 'Hellow', 'a', 'a', '075.webp', 'skills', 'Special_skill', '1999-01-01', 'dave', 1, 'None'),
(37, 'Tutor', 'Elementary', 'I am working on frontend for over 3 yearsa', '500', 'nichol20', 1, 'Mark Nichol', 'R', 'Quezon', 'man1.png', 'skills', 'Special_skill', '1999-01-01', 'nichol20', 1, 'None'),
(36, 'test1', 'test1', 'test1', '200', 'dave', 1, 'Hellow', 'a', 'a', '075.webp', 'skills', 'Special_skill', '1999-01-01', 'dave', 1, 'None');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(20) NOT NULL,
  `receiver` varchar(20) NOT NULL,
  `msg` varchar(500) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `sender`, `receiver`, `msg`, `timestamp`, `status`) VALUES
(21, 'dave', 'nichol200', 'You have been declined on job title test1', '2021-12-07 05:29:45', 1),
(20, 'nichol20', 'nichol200', 'You have been declined on job title Web Build', '2021-12-07 05:28:44', 1),
(19, 'nichol20', 'nichol200', 'You have been declined on job title vvv', '2021-12-07 05:28:05', 1),
(14, 'nichol20', 'nichol200', 'asdfasdf', '2021-12-01 15:54:01', 1),
(15, 'dave', 'nichol20', 'Yellow!', '2021-12-07 04:01:44', 1),
(13, 'nichol200', 'nichol20', 'hjello', '2021-12-01 15:53:28', 1),
(22, 'dave', 'nichol200', 'You have been declined on job title asdf', '2021-12-07 05:35:23', 1),
(23, 'nichol20', 'nichol200', 'You have been declined on job title Web Build', '2021-12-07 05:56:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `review_table`
--

DROP TABLE IF EXISTS `review_table`;
CREATE TABLE IF NOT EXISTS `review_table` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `user_rating` int(5) NOT NULL,
  `user_review` varchar(100) NOT NULL,
  `datetime` int(11) NOT NULL,
  `rjob_id` int(3) DEFAULT NULL,
  PRIMARY KEY (`review_id`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review_table`
--

INSERT INTO `review_table` (`review_id`, `user_name`, `user_rating`, `user_review`, `datetime`, `rjob_id`) VALUES
(63, 'nichol200', 4, 'Veryy GOOD!', 1638856695, 17),
(62, 'nichol200', 3, 'bery goed', 1638698611, 36),
(61, 'ppikoy', 1, 'Verrry Pooor', 1638698153, 16),
(60, 'nichol200', 5, 'Verrrrrrryyyyyy Goooooooood', 1637499387, 17),
(59, 'nichol200', 2, 'awdsdaw', 1636698724, 16),
(58, 'nichol200', 1, 'OMG!', 1636698570, 17),
(57, 'nichol200', 5, 'hellow', 1636697249, 16);

-- --------------------------------------------------------

--
-- Table structure for table `selected`
--

DROP TABLE IF EXISTS `selected`;
CREATE TABLE IF NOT EXISTS `selected` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_username` varchar(20) NOT NULL,
  `job_id` varchar(20) NOT NULL,
  `e_username` varchar(20) NOT NULL,
  `price` varchar(10) NOT NULL,
  `valid` tinyint(2) NOT NULL,
  `statss` int(1) NOT NULL DEFAULT '1',
  `titlE` varchar(100) NOT NULL,
  `typE` varchar(100) NOT NULL,
  `namE` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `selected`
--

INSERT INTO `selected` (`id`, `f_username`, `job_id`, `e_username`, `price`, `valid`, `statss`, `titlE`, `typE`, `namE`) VALUES
(26, 'nichol200', '16', 'nichol20', '', 1, 1, 'vvv', 'vvv', 'Mark Nichol, Quezon'),
(25, 'nichol200', '17', 'nichol20', '', 0, 1, 'Web Build', 'Frontend', 'Mark Nichol, Quezon'),
(23, 'nichol200', '36', 'dave', '', 0, 1, 'test1', 'test1', 'Hellow, a'),
(24, 'ppikoy', '16', 'nichol20', '', 0, 1, 'vvv', 'vvv', 'Mark Nichol, Quezon');

-- --------------------------------------------------------

--
-- Table structure for table `sign_up`
--

DROP TABLE IF EXISTS `sign_up`;
CREATE TABLE IF NOT EXISTS `sign_up` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) NOT NULL,
  `mname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `Gender` text NOT NULL,
  `age` varchar(100) NOT NULL,
  `bdate` date NOT NULL,
  `cnum` varchar(11) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `username` varchar(16) NOT NULL,
  `image` varchar(255) NOT NULL,
  `valid` varchar(255) NOT NULL,
  `ncc` varchar(255) NOT NULL,
  `password` varchar(16) NOT NULL,
  `address` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
