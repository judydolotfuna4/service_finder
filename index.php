<?php include('server.php');
if (isset($_SESSION["Username"])) {
  $username = $_SESSION["Username"];
  if ($_SESSION["Usertype"] == 1) {
    header("location: freelancerProfile.php");
  } else {
    header("location: employerProfile.php");
  }
} else {
  $username = "";
  //header("location: index.php");
}

?>


<!DOCTYPE html>
<html>

<head>
  <title>Service Finder</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
  <link rel="stylesheet" href="asset/fontawesome/css/all.min.css">
  <link rel="stylesheet" href="asset/css/mystyle.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500;600;700;800&display=swap" rel="stylesheet">
  <style>
      body {
        font-family: 'Kanit', sans-serif;
      }
      .logo img {
      height: 60px;
    }
      .banner {
        text-align:center;
        background: url(image/banner.jpg) no-repeat;
        background-size: cover; 
        height: 90vh;
        background-position: center;
      }
      .gradient {
        background: linear-gradient( 
        120deg,#343a40,#6299a4);
        color: #fff;
      }
      .portfolio-navbar .navbar-nav .nav-link {
        font-weight: 600;
        font-size: 20px;
        padding: 3px 20px;
        border: 1px solid darkgray;
        border-radius: 10px;
        color: #fff;
        margin-left: 10px;
      }
      .portfolio-navbar .navbar-nav .nav-link:hover {
        background: lightgray;
        color: #000000;
      }
      .portfolio-navbar .navbar-nav .nav-item {
        padding-right: 10px;
      }
      h1.mb-5 {
        font-size: 40px;
        font-weight: bold;
        color: #f5f6f8;
      }
      .btn-getstarted {
        display: inline-block;
        font-weight: bolder;
        text-align: center;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-color: transparent;
        border: 1px solid darkgray;
        border-radius: 10px;
        color: #fff;
        padding: 10px 23px;
        line-height: 1.5;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
      }
      .btn-getstarted:hover{
        background: lightgray;
        color: #000000;
      }
      .landing-page .skills {
        padding: 70px 0;
      }
      .landing-page .heading {
        margin-bottom: 30px;
      }
      h2:before {
        content: " ";
        width: 15%;
        height: 3px;
        background: #495057;
        display: inline-block;
        margin: 0 10px 10px 0;
      }
      h2:after {
        content: " ";
        width: 15%;
        height: 3px;
        background: #495057;
        display: inline-block;
        margin-bottom: 10px;
        margin: 0 0 10px 10px;
      }
      .card-header img {
        height: 80px;
      } 
      .our-mission {
        background: linear-gradient( 
        120deg,#343a40,#6299a4);
        padding: 70px 0;
        color: #000000;
      }
      .our-mission h3 {
        padding-top:20px;
        font-size: 30px;
        color: #fff;
      }
      .our-mission .btn-offering {
        border: 1px solid darkgray;
        border-radius: 10px;
        color: #fff;
        margin-left: 225px;
        padding: 10px 20px;
      }
      .our-mission .btn-offering:hover {
        background: lightgray;
        color: #000000;
      }
      .system-desc p {
        font-size: 22px;
        text-align: justify;
        color: #fff;
      }
      .page-footer {
        padding: 15px 0;
        background: darkgray;
      }
      .modal-header {
        background: linear-gradient( 
        120deg,#343a40,#6299a4);
        color: #fff;
      }
    </style>
</head>

<body>
  <!--Navbar menu-->
  <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-light portfolio-navbar gradient py-0" id="nav">

    <div class="container">
      <a class="navbar-brand logo" href="index.php"><img src="image/logo.png" width="250" alt="Logo"></a><button data-toggle="collapse" class="navbar-toggler" data-target="#navbarNav"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
      <div class="collapse navbar-collapse nav-dropdown" id="navbarNav">
        <ul class="nav navbar-nav ml-auto">
          <!-- <li class="nav-item" role="presentation"><a class="nav-link" href="elogin.php">Login</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="signup.php">Sign up</a></li>
                    <div class="collapse navbar-collapse" id="navcol-1">
                        <a class="btn ml-auto rounded-pill btn-font-size px-4" role="button" href="register.php" style="background: rgb(116, 156, 143);color:#fff;">Register as an Employee</a>
                    </div> -->
          <a class="nav-link" href="login.php" role="button" >Login</a>
          
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Register
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="signup.php">Register as an Employer</a></li>
              <li><a class="dropdown-item" href="register.php">Register as an Employee</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!--End Navbar menu-->

  <!--slider-->
  <header class="masthead text-white text-center banner">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-xl-9 mx-auto desc" style="margin-top:230px;">
          <h1 class="mb-5">Find &amp; Explore best service provider near you.</h1>
        </div>
        <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
          <form>
            <div class="form-row justify-content-center">
              <button type="button" class="btn-getstarted" onclick="document.location='login.php'">Get Started</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </header>
  <main class="page landing-page">
    <section class="portfolio-block skills">
      <div class="container">
        <div class="heading">
          <h2>Your Partner to easily find services</h2>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="card special-skill-item border-0">
              <div class="card-header bg-transparent border-0"><img src="image/5e257b911f3a3.png" alt=""></div>
              <div class="card-body">
                <p class="card-text">Locally Extensive Talent Pool</p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card special-skill-item border-0">
              <div class="card-header bg-transparent border-0"><img src="image/handshake-icon.png" alt=""></div>
              <div class="card-body">
                <p class="card-text">Trustworthiness, integrated, proved by employee referals.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card special-skill-item border-0">
              <div class="card-header bg-transparent border-0"><img src="image/employees.png" alt=""></div>
              <div class="card-body">
                <p class="card-text">Dedicated, committed and reliable regardless of specialties.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="our-mission">
      <div class="container">
        <div class="row">
          <div class="col">
            <h3>Service Finder</h3>
          </div>
          <div class="col">
          <!-- <button type="button" class="btn btn-lg btn-offering">We're Offering Services! &nbsp;<i class="fas fa-arrow-right"></i></button> -->
          <!-- Button trigger modal -->
          <button type="button" class="btn btn-offering" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
          We're Offering Services! &nbsp;<i class="fas fa-arrow-right"></i>
          </button>
    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Offered Services:</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
            <div class="modal-body">
            <ul class="services">
              <li>Tutor</li>
              <li>Manicurist</li>
              <li>Carpenter</li>
              <li>Construction Worker</li>
              <li>Electrician</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
          </div>
        </div>
        <div class="system-desc">
        <p>Join our mission to provide reliable, honest and passionate employee in a digital service finder around the vicinity of Irosin, Sorsogon - all while building and helping you to have an expanded options regarding a specific field of expertee needed in an individual.</p>
        </div>
      </div>
    </section>
  </main>
    <footer class="page-footer">
        <div class="container">
           <p>© 2021, All Rights Reserved</p>
        </div>
    </footer>



  <script src="assets/js/jquery-3.2.1.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/js/jquery.min.js"></script>
</body>

</html>