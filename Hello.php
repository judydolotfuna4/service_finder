<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="asset/fontawesome/css/all.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500;600;700;800&display=swap" rel="stylesheet">   
</head>
<style>
   body {
    font-family: 'Kanit', sans-serif;
  }
  .card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-clip: border-box;
    border-radius: .25rem;
		padding-bottom: 30px;
		box-shadow:
  0 2.8px 2.2px rgba(0, 0, 0, 0.034),
  0 6.7px 5.3px rgba(0, 0, 0, 0.048),
  0 12.5px 10px rgba(0, 0, 0, 0.06),
  0 22.3px 17.9px rgba(0, 0, 0, 0.072),
  0 41.8px 33.4px rgba(0, 0, 0, 0.086),
  0 15px 15px rgba(0, 0, 0, 0.12);
  margin-bottom: 40px;
}
  .card-body p {
    display: flex;
    justify-content: center;
    text-align: center;
  }
  .card-body a {
    display: flex;
  justify-content: center;
  flex-direction: row;
  }
  .btn-tologin {
      border: 2px solid rgba(116, 156, 143);
    color: #000;
    padding: 5px 25px;
    }
    .btn-tologin:hover {
      background: #78e08f;
      color: #000;
    }
</style>
<body>
  <div class="container">
    <div class="mx-auto" style="width: 335px;">
      <div class="card" style="width: 400px;margin-top: 100px;">
        <div class="card-body">
          <p class="card-text">Wait for the approval of your account. We will send you a message.</p>
          <a href="logout.php" class="btn btn-tologin">Click to Login</a>
        </div>
      </div>
    </div>
  </div>
    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/theme.js"></script>
</body>
</html>